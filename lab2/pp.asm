section .data
arr dw 1, 2, -3, -4, 5, 6, -7, 8, 9, 10, -11, -12
pos_sum_msg db "Сумма положительных: %d", 0Ah, 0
neg_sum_msg db "Сумма отрицательных: %d", 0Ah, 0
bit7_msg db "1 в 7 бите присутствует", 0Ah, 0
no_bit7_msg db "1 в 7 бите отсутствует", 0Ah, 0
err_msg db "Переполнение суммы", 0Ah, 0

section .text
extern printf
global main

main:
push rbp
mov rbp, rsp
mov ecx, 12 ; количество элементов в массиве
mov ebx, arr ; указатель на начало массива
xor edx, edx ; сумма положительных элементов

lp1:
    movsx eax, word [ebx] ; загружаем значение из памяти с расширением знака до 32 бит
    cmp eax, 0
    jg pos1
    jmp cont1

    pos1:
        add edx, eax
       cmp edx, 127
       jg err
    cont1:
        add ebx, 2
        loop lp1
        
mov rdi, pos_sum_msg
mov esi, edx 
mov eax, 0 
call printf 

mov ecx, 12 ; количество элементов в массиве
mov ebx, arr ; указатель на начало массива
xor edx, edx ; сумма отрицательных элементов

lp2:
    movsx eax, word [ebx] ; загружаем значение из памяти с расширением знака до 32 бит
    cmp eax, 0
    js pos2
    jmp cont2

    pos2:
        add edx, eax
        cmp edx, -128
        jl err

    cont2:
        add ebx, 2 
        loop lp2

mov rdi, neg_sum_msg
mov esi, edx ; загружаем сумму отрицательных элементов
mov eax, 0 
call printf 

mov edx, esi
shr edx, 7 ; сдвигаем сумму отрицательных элементов на 7 битов вправо
and edx, 1 ; проверяем значение 7-го бита
cmp edx, 1
je bit7
mov rdi, no_bit7_msg
call printf 
jmp end
    bit7:
        mov rdi, bit7_msg
        call printf 
        jmp end
        
err:
    mov rdi, err_msg
    mov rax, 0           
    call printf
    jmp end

end:
mov eax, 0
pop rbp
ret
