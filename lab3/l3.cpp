#include <iostream>

using namespace std;

int main() {
    int n = 10;
    int a[n] = {1, -15, 4, 1, -5, 6, 9, -2, 0, 3};
    int max_el = a[0];
    int min_el = a[0];
    int max_pos = 0;
    int min_pos = 0;

    for (int i = 0; i < n; ++i) {
        asm(
            "mov %[max_el], %%eax\n\t"
            "cmp %[a_i], %%eax\n\t"
            "jge max_check\n\t"
            "mov %[a_i], %[max_el]\n\t"
            "mov %[i], %[max_pos]\n\t"
            "max_check:\n\t"
            "mov %[min_el], %%eax\n\t"
            "cmp %[a_i], %%eax\n\t"
            "jle min_check\n\t"
            "mov %[a_i], %[min_el]\n\t"
            "mov %[i], %[min_pos]\n\t"
            "min_check:\n\t"
            : [max_el] "+r" (max_el), [min_el] "+r" (min_el), [max_pos] "+r" (max_pos), [min_pos] "+r" (min_pos)
            : [a_i] "r" (a[i]), [i] "r" (i)
            : "%eax"
        );
    }

    cout << "Максимальный элемент: " << max_el << ". Индекс в массиве: " << max_pos << endl;
    cout << "Минимальный элемент: " << min_el << ". Индекс в массиве: " << min_pos << endl;

    return 0;
}

