#!/bin/bash

echo "Автор: Давыдченко П.Ю. 729-1"
echo "Название программы: Узнать UID и группы пользователя"
echo "Описание программы: Скрипт запрашивает имя пользователя, проверяет его наличие в системе, выводит UID и имена групп, в которые пользователь входит"

# Создание пользователя polina
useradd polina
# Установка пароля пользователя polina
echo "polina:1234" | chpasswd
# Добавление пользователя polina в группу sudo
usermod -aG sudo polina

while true; do
  read -p "Введите имя пользователя: " username

  # Проверка, что пользователь с таким именем существует
  if id -u "$username" >/dev/null 2>&1; then
    # Получение UID пользователя
    uid=$(id -u "$username")
    echo "UID пользователя $username: $uid"

    # Получение и вывод имен групп, в которые входит пользователь
    echo "Группы пользователя $username:"
    id -Gn $username | tr ' ' '\n' | while read groupname; do
      if [ $(id -gn "$username") = "$groupname" ]; then
        echo "  $groupname (основная группа)"
      else
        echo "  $groupname"
      fi
    done

    # Предложение продолжить или завершить работу скрипта
    read -p "Хотите продолжить? (y/n): " answer
    if [ "$answer" != "y" ]; then
      break
    fi
  else
    echo "Пользователь $username не существует. Попробуйте еще раз."
  fi
done
